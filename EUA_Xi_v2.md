# EUA Xi v2.0

## Bill of materials
(excluding 3D-printed parts)

| Pcs  | Item                                                         | Link                                                         |
| ---- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 4    | "Grabbers", machined, aluminium                              |                                                              |
| 8    | Ball bearings                                                | https://dk.rs-online.com/web/p/kuglelejer/6125767            |
| 8    | Timing belt pulley 20T, deling: 2.5                          | https://dk.rs-online.com/web/p/tandremme-tandremskiver/2864547 |
| 3    | Timing belt 6mmx120mm, 48T, deling 2.5                       | https://dk.rs-online.com/web/p/tandremme/4744984             |
| 1    | Timing belt 6mmx145mm, 58T, deling 2.5                       | https://dk.rs-online.com/web/p/tandremme/4744990             |
| 2    | Carbonfiber rod, 8mm diameter, 280mm long                    |                                                              |
| 1    | Carbonfiber rod, 8mm diameter, 287mm long                    |                                                              |
| 12   | Maskinskrue M3, invendigt hex, 10mm gevind-længde            |                                                              |
| 4    | Maskinskrue M4, undersænket, 35mm (total-længde)             |                                                              |
| 4    | Maskinskrue M4, Hexagonalt hovede (udvendig), 30mm gevind-længde |                                                          |
| 8    | Maskinskrue M4, invendigt hex, 50mm gevind-længde            |                                                              |
| 8    | Maskinskrue M4, invendigt hex, 40mm gevind-længde            |                                                              |
| 4    | Maskinskrue M6, invendigt hex, 16mm gevind-længde            |                                                              |
| 24   | Låsemøtrik M4                                                |                                                              |
